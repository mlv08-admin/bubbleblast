package it.prisma;

public class CheckCollision {

    public CheckCollision(Grid gd) {
        String[][] gameframe = gd.getGameframe();

    }

    // controllo sulle bolle e regole del gioco
    public  void checkState(int x, int y,String[][] gameframe) {

        switch (gameframe[x][y]) {
            case "2":
                gameframe[x][y] = "-";
                checkOr(x, y, gameframe);
                checkVer(x, y, gameframe);

                break;
            case "0":
                gameframe[x][y] = "1";
                break;
            case "1":
                gameframe[x][y] = "2";


                break;
        }
    }

    public  void checkVer(int x, int y,String[][] gameframe) {
        for (int i = x; i < gameframe.length; i++) {
            if (!gameframe[i][y].equals("-")) {
                checkState(i, y,gameframe);
                break;
            }

        }
        for (int i = x; i >= 0; i--) {
            if (!gameframe[i][y].equals("-")) {
                checkState(i, y,gameframe);
                break;
            }

        }


    }

    public  void checkOr(int x, int y,String[][] gameframe) {
        for (int i = y; i < gameframe[x].length; i++) {
            if (!gameframe[x][i].equals("-")) {
                checkState(x, i,gameframe);
                break;
            }

        }
        for (int i = y; i >= 0; i--) {
            if (!gameframe[x][i].equals("-")) {
                checkState(x, i,gameframe);
                break;
            }

        }

    }
}
