package it.prisma;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class Grid {

    private  final String[][] gameframe = new String[6][5];
    private  final String[] state = {"0", "1", "2", "-"};  // 0 == sgonfia  1 == gonfia  2 == che sta per esplodere

    // metodo get della griglia
    public  String[][] getGameframe() {
        return gameframe;
    }

    //metodo get della griglia per salvarlo su file txt
    public String getGridString(){
        StringBuilder  result  = new StringBuilder();

        Arrays.stream(gameframe).forEach((i) -> {
            Arrays.stream(i)
                    .forEach((j) -> result.append(j).append("\t"));
            result.append("\n");
        });

        return String.valueOf(result);
    }


    //generazione della grilgia con posizione delle palline in modo casuale
    public  void genareteGrid() {
        IntStream.range(0, gameframe.length)
                .forEach(x -> Arrays.setAll(gameframe[x], y -> state[randomChar(state)]));
    }
    public  int randomChar(String  [] state) {  // generazione palline casuali
        Random r = new Random();
        int randomc = r.nextInt(state.length);
        return randomc;
    }


    // stampa della griglia di gioco con le bolle inserite casualmente nei vari stati
    public  void printGrid() {
        Arrays.stream(gameframe)
                .forEach((i) -> {
            Arrays.stream(i)
                    .forEach((j) -> System.out.print(j + " "));
            System.out.println();
        });

    }






}



