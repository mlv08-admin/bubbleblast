package it.prisma;

import java.util.Scanner;

public class InputPlayer {

    private  int x ;
    private  int y;
    private final Grid gd;
    private final  String[][] gameframe;

    public InputPlayer(Grid gd) {
        this.gd = gd;
        gameframe = gd.getGameframe();
    }

    //get e set per le variabili x e y che indicano le cordinate nella griglia
    public  int getX() {
        return x;
    }
    public  void setX(int x) {
        this.x = x;
    }
    public  int getY() {
        return y;
    }
    public  void setY(int y) {
        this.y = y;
    }


    // metodo per inserite l input da terminale e dare le cordinate aalla griglia per far gonfiare o scoppiare una bolla
    public  void inputCords() {
        Scanner mycord = new Scanner(System.in); //imput del gioco
        System.out.println("enter raw: ");
        x = mycord.nextInt();
        System.out.println("enter column: ");
        y = mycord.nextInt();
        System.out.println("you insert the cordinats: " + x + ", " + y);
        System.out.println();
        setY(y);
        setX(x);

    }

}
