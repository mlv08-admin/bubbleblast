package it.prisma;

import java.util.Arrays;
import java.util.stream.IntStream;

public class MoveCount {

    private final  Grid gd = new Grid();
    private final CheckCollision check = new CheckCollision(gd);
    private final InputPlayer inpt = new InputPlayer(gd);

    // meteodo per calcolare le palle rimanenti nella griglia
    public int remainingBalls(String[][] gf){
        int power2 = 0;
        int power1 =0;
        int power0 =0;
        int sumPower  ;
        for(String[] a : gf){
            for(String b : a){
                switch (b) {
                    case "0":
                        power0++;
                        break;
                    case "1":
                        power1++;
                        break;
                    case "2":
                        power2++;
                        break;
                }
            }
        }
        sumPower = power0 + power1 + power2;


        return  sumPower;

    }

    // metodo per calcolare la forza di una bolla a una determinata posizione // todo
    public int calculatePower(String[][] gameframe, int x, int y){
        int power = 0;


        power += Arrays.stream(gameframe)
                .flatMap(Arrays::stream)
                .map(b -> inpt.getX() )
                .filter(b -> x == inpt.getX())
                .filter(a -> gameframe[x][y].equals("2"))
                .mapToInt(b -> 2)
                .sum();

        power += Arrays.stream(gameframe)
                .flatMap(Arrays::stream)
                .map(b -> inpt.getY() )
                .filter(b -> y == inpt.getY())
                .filter(a -> gameframe[x][y].equals("2"))
                .mapToInt(b -> 2)
                .sum();

        power += Arrays.stream(gameframe)
                .flatMap(Arrays::stream)
                .map(b -> inpt.getX() )
                .filter(b -> x == inpt.getX())
                .filter(a -> gameframe[x][y].equals("1"))
                .mapToInt(b -> 1)
                .sum();

        power += Arrays.stream(gameframe)
                .flatMap(Arrays::stream)
                .map(b -> inpt.getY() )
                .filter(b -> y == inpt.getY())
                .filter(a -> gameframe[x][y].equals("1"))
                .mapToInt(b -> 1)
                .sum();





        /*
        for (int i = y; i < gameframe[x].length; i++) {
            if (gameframe[x][i].equals("2")) {
                power+=2;


            }
            if (gameframe[i][y].equals("1")) {
                power++;

            }

        }

        for (int i = y; i >= 0; i--) {
            if (gameframe[x][i].equals("2")) {
                power+=2;

            }
            if (gameframe[i][y].equals("1")) {
                power++;

            }

        }
        for (int i = x; i < gameframe.length; i++) {
            if (gameframe[i][y].equals("2")) {
                power+=2;
            }
            if (gameframe[x][y].equals("1")) {
                power++;

            }

        }
        for (int i = x; i >= 0; i--) {
            if (gameframe[i][y].equals("2")) {
                power += 2;

            }
            if (gameframe[i][y].equals("1")) {
                power++;

            }

        }
        if(gameframe[x][y].equals("2")){
            power+=2;
        }
        if(gameframe[x][y].equals("1")){
            power++;
        }*/

        return power;

    }


    // metodo per scegliere la palla migliore da usare per ogni mossa
    public int[] BestBall(String[][] grid){
        int r = 0;
        int c = 0;
        int[] result = new int[2];
        for(int row = 0; row < 6  ; row++){
            for(int column = 0 ; column < 5; column++){
                if( grid[row][column].equals("2") & calculatePower(grid,row,column) >= calculatePower(grid,r,c)){
                    r = row;
                    c = column;
                }
            }
        }
        result[0]  = r;
        result[1] = c;

        return  result;
    }

    // metodo per creare una copia della griglia originale
    public String[][] copyGrid(String[][]  gameframe){


        String[][] gridClone = new String[6][5];
        IntStream.range(0, gameframe.length)
                .forEach(x -> Arrays.setAll(gameframe[x], y -> (gridClone[x][y] = gameframe[x][y])));
        return gridClone;

     /*
        for(int row = 0; row < 6; row++){
            for(int column = 0; column < 5; column++){
                gridClone[row][column] = gameframe[row][column] ;
            }
        }
        return gridClone;    */

    }

    // metodo per calcolare il numero minimo di mosse per far terminare il gioco
    public int calculateMinmoves(String[][] griglia){
        int countermoves = 0;
        while(remainingBalls(griglia)!=0) {
            int[] cordinate = BestBall(griglia);
            check.checkState(cordinate[0], cordinate[1], griglia);
            countermoves++;

        }
        System.out.println("numero di mosse minimo: "+ countermoves);
        return countermoves;
    }

}
