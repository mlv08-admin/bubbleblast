package it.prisma;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class MoveSavier {

    private final FileWriter flw;

    // costruttore per il salvataggio
    public MoveSavier() throws IOException {
        flw = new FileWriter("BUBBLE BLAST.txt");
        flw.write("Foglio riassuntivo dei comandi del giocatore \n\n\n\n");
        flw.write("Griglia di partenza  \n\n\n\n\n");
        flw.write("\r\n");
        flw.close();
    }

    // metodo per salvare le cordinate inserite dal giocatore in un file txt
    public  void saveSolution(InputPlayer inp) throws Exception {
        String cordX = Integer.toString(inp.getX());
        String cordY = Integer.toString(inp.getY());
        String inputPlayer = "Cordinate inserite dal giocate : riga / colonna = " + cordX + " : " + cordY+ "\n" ;
        OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream("BUBBLE BLAST.txt",true));
        out.append(inputPlayer);
        out.append("\n");
        out.close();
    }
    
    // metodo per salvare la griglia modificata dall'input del giocatore in un file txt
    public  void saveGrid(Grid gs) throws Exception {
        OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream("BUBBLE BLAST.txt",true));
        out.append(gs.getGridString());
        out.append("\n");
        out.close();
    }
}
