package it.prisma;

public class StartGame {
    public static void main(String[] args) throws Exception {


        System.out.println(" \n" +
                "o.oOOOo.          o     o     o       \n" +
                " o     o         O     O     O        \n" +
                " O     O         O     O     o        \n" +
                " oOooOO.         o     o     O        \n" +
                " o     `O O   o  OoOo. OoOo. o  .oOo. \n" +
                " O      o o   O  O   o O   o O  OooO' \n" +
                " o     .O O   o  o   O o   O o  O     \n" +
                "o.oOOOo.  `ooO'o `OoO' `OoO' Oo `OoO' \n" +
                " o     o  O                           \n" +
                " O     O  o                O          \n" +
                " oOooOO.  O               oOo         \n" +
                " o     `O o  .oOoO' .oOo   o          \n" +
                " O      o O  O   o  `Ooo.  O          \n" +
                " o     .O o  o   O      O  o          \n" +
                " `OooOO'  Oo `OoO'o `OoO'  `oO        \n" +
                "                                      \n" +
                "                                      \n");
        System.out.println();


        Grid g = new Grid();
        g.genareteGrid();
        g.printGrid();
        InputPlayer inp = new InputPlayer(g);
        CheckCollision check = new CheckCollision(g);
        MoveSavier mv = new MoveSavier();
        MoveCount mc = new MoveCount();
        String[][] copy = mc.copyGrid(g.getGameframe());
        System.out.println();
        mc.calculateMinmoves(copy);
        mv.saveGrid(g);
        System.out.println("le righe vanno da 0 a 5 mentre le colonne da 0 a 4");

        System.out.println("*****************************************************************************************");


        while (mc.remainingBalls(g.getGameframe())!=0) {
            int[] bestBall = mc.BestBall(g.getGameframe());
            System.out.println("la miglior bolla ha cordinate : "    + bestBall[0] + " : " + bestBall[1]);
            System.out.println();
            System.out.println();
            int x ;
            int y ;
            do {
                inp.inputCords();
                x = inp.getX();
                y = inp.getY();
                System.out.println();
                System.out.println(x > 5 || y > 4 ? "CORDINATE NON VALIDE" : "");
                System.out.println();

            }
            while((x > 5 && x < 0 ) || (  x < 0    &&   y > 4));

            check.checkState(x, y, g.getGameframe());
            g.printGrid();
            mv.saveSolution(inp);
            System.out.println();
            mv.saveGrid(g);
            System.out.println();
        }
    }
}
